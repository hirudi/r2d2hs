Introduction
============
This is a R2D2 3D printed robot designed/derived by [Hirudi]/[Sicnova3D]'s crew to thanks [Obijuan's] contribution to the [World Technology Heritage] ;). 

This printbot is designed to fit some electronics elements of the [BQ robotics kit].


Printed parts
=============


Electronic parts
================


Software
========


Licence
=======
+ SW: [GPLv3] 
+ HW: [CERN OHL]


Authors
=======
+ Adrián Hueso
+ Joseba Senosiain

[Hirudi]:http://hirudi.sicnova3d.com/ "Hirudi"
[Sicnova3D]:http://sicnova3d.com/ "Sicnova3D"
[Obijuan's]:http://www.iearobotics.com/wiki/index.php?title=Juan_Gonzalez:Main "Obijuan"
[World Technology Heritage]:http://www.iearobotics.com/wiki/index.php?title=Obijuan_Academy "World Technology Heritage"
[BQ robotics kit]:http://www.bq.com/es/productos/kit-robotica.html?utm_content=buffer4cbc2&utm_medium=social&utm_source=plus.google.com&utm_campaign=buffer "BQ robotics kit"
[GPLv3]:http://www.gnu.org/copyleft/gpl.html "GPLv3"
[CERN OHL]:http://www.ohwr.org/projects/cernohl/wiki "CERN OHL"





